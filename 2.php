<!DOCTYPE html>
<html>
<head>
	<title>Soal No. 2</title>
<?php
    include "_partial/head.php";
  ?>
</head>
<body>
	<div class="container">
		<h2>Form Validasi Username dan Password</h2>
<form action="" method="POST" onsubmit="return validasi(this)">
	<table>
		<tr>
			<td>
				Username
			</td>
			<td>
				:
			</td>
			<td>
				<input type="text" name="nama"><br>
			</td>
		</tr>
		<tr>
			<td>
				Password
			</td>
			<td>
				:
			</td>
			<td>
				<input type="text" name="pass"><br>
			</td>
		</tr>
		<tr>
				<td></td>
				<td></td>
				<td>
					<input style="float: right;" type="submit" name="submit" value="kirim">
				</td>
			</tr>
	</table>
</form>
</div>
<script type="text/javascript">
	function validasi(form){
			pola_username=/^[a-zA-Z][a-zA-Z0-9]{4,8}$/;
			pola_password = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
			if(form.nama.value==""){
				alert("Nama Tidak Boleh Kosong");
				form.nama.focus();
				return false;
			}
			if(!pola_username.test(form.nama.value)){
				alert("Penulisan username tidak valid (min 5, maks 9)");
				form.nama.focus();
				return false;
			}

			if(form.pass.value==""){
				alert("Password Tidak Boleh Kosong");
				form.pass.focus();
				return false;
			}
			if(!pola_password.test(form.pass.value)){
				alert("Penulisan password tidak valid (min 8)");
				form.pass.focus();
				return false;
			}
			return true;
		}
</script>
<script type="text/javascript" src="asset/js/jquery.min.js"></script>
	<script src="javascript/jquery-3.4.1.min.js"></script>
	<script src="javascript/jquery.js"></script>
    <script type="text/javascript" src="asset/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="asset/js/sweetalert.min.js"></script>
    <script src="dasset/js/bootstrap-datepicker.js"></script>
    
<?php
    include "_partial/footer.php";
  ?>
</body>
</html>