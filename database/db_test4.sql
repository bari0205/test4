-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 22, 2020 at 11:31 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_test4`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `code_brand` varchar(100) COLLATE latin1_bin NOT NULL,
  `name` varchar(100) COLLATE latin1_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `code_brand`, `name`) VALUES
(1, 'BD1', 'HONDA'),
(2, 'BD2', 'YAMAHA'),
(3, 'BD3', 'SUZUKI'),
(4, 'BD9', 'kawasaki'),
(5, 'BD5', 'Fajar');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(255) NOT NULL,
  `code_cus` varchar(255) COLLATE latin1_bin NOT NULL,
  `name` varchar(100) COLLATE latin1_bin NOT NULL,
  `address` varchar(1000) COLLATE latin1_bin NOT NULL,
  `phone` varchar(255) COLLATE latin1_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `code_cus`, `name`, `address`, `phone`) VALUES
(1, 'KD123', 'BARII', 'Jl. Ki haji dewatara yogyakarta Indonesia', '08173246124'),
(2, 'KD1', 'LUQMAN', 'Tugu Yogyakarta, Kota Yogyakarta', '0827341344'),
(3, 'KD2', 'BRIAN', 'Jl, Dr. Soepome, Warungboto', '087615349222');

-- --------------------------------------------------------

--
-- Table structure for table `motorcycle`
--

CREATE TABLE `motorcycle` (
  `id` int(11) NOT NULL,
  `code_motor` varchar(100) COLLATE latin1_bin NOT NULL,
  `name` varchar(100) COLLATE latin1_bin NOT NULL,
  `code_brand` varchar(100) COLLATE latin1_bin NOT NULL,
  `image` varchar(9999) COLLATE latin1_bin NOT NULL DEFAULT 'default.jpg',
  `color` varchar(100) COLLATE latin1_bin NOT NULL,
  `spesification` varchar(9999) COLLATE latin1_bin NOT NULL,
  `price` int(255) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Dumping data for table `motorcycle`
--

INSERT INTO `motorcycle` (`id`, `code_motor`, `name`, `code_brand`, `image`, `color`, `spesification`, `price`, `stock`) VALUES
(1, 'PD1', 'JUPUTER', 'BD3', 'motor1.png', 'Hitam', 'Rem cakram\r\nAman saat berkendara', 15000000, 100),
(2, 'PD2', 'BEAT', 'BD1', 'motor2.png', 'Merah', 'Bagus Banget aman saat ingin berkendara', 23000000, 100);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `code_motor` varchar(100) COLLATE latin1_bin NOT NULL,
  `qty` int(11) NOT NULL,
  `dtm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `code_motor`, `qty`, `dtm`) VALUES
(1, 'PD1', 1, '2020-08-22 10:26:45');

--
-- Triggers `stock`
--
DELIMITER $$
CREATE TRIGGER `TG_tambahStock` AFTER INSERT ON `stock` FOR EACH ROW BEGIN
	UPDATE motorcycle SET stock = stock + NEW.qty
    WHERE code_motor = NEW.code_motor;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `trx`
--

CREATE TABLE `trx` (
  `id` int(11) NOT NULL,
  `code_cus` varchar(100) COLLATE latin1_bin NOT NULL,
  `code_motor` varchar(100) COLLATE latin1_bin NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `dtm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

--
-- Dumping data for table `trx`
--

INSERT INTO `trx` (`id`, `code_cus`, `code_motor`, `qty`, `price`, `total`, `dtm`) VALUES
(1, 'KD1', 'PD1', 1, 15000000, 15000000, '2020-08-22 10:26:17');

--
-- Triggers `trx`
--
DELIMITER $$
CREATE TRIGGER `TG_kurangStock` AFTER INSERT ON `trx` FOR EACH ROW BEGIN
	UPDATE motorcycle SET stock = stock - NEW.qty
    WHERE code_motor = NEW.code_motor;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motorcycle`
--
ALTER TABLE `motorcycle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trx`
--
ALTER TABLE `trx`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `motorcycle`
--
ALTER TABLE `motorcycle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `trx`
--
ALTER TABLE `trx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
