 <?php
    include "koneksi.php";
  ?>
<!DOCTYPE>
<html>
<head>
	<title>Soal No. 4</title>

	<?php
    include "_partial/head.php";
  ?>
        
</head>
<body>
	<div style="text-align: center;">
		<h3>Tambah Brand</h3>
	</div>
	<form class="form-horizontal" id="form" action="req_tbrand.php" method="POST" enctype="multipart/form-data">
		  <fieldset>
		    <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Kode Brand</label>
		      <div class="col-lg-10">
		        <input type="text" class="form-control" id="kd_brand" name="kd_brand" value="BD"  >
		      </div>
		    </div>

		     <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Nama Brand</label>
		      <div class="col-lg-10">
		        <input type="text" class="form-control" placeholder="Masukan Nama" id="nama_brand" name="nama_brand">
		      </div>
		    </div>
		     
		    <div class="form-group">
		      <div class="col-lg-10 col-lg-offset-2">
		        <input type="submit" name="submit" class="btn btn-primary" value="Tambah">
		      </div>
		    </div>
		  </fieldset>
		</form> 
	<?php
    include "_partial/footer.php";
  ?>
</body>
	
</html>