<!DOCTYPE html>
<html>
<head>
	<title>Soal no 1</title>
	<?php
    include "_partial/head.php";
  ?>
</head>
<body>	
<?php error_reporting(0); ?>
<div class="container">
<div style="width: 50; float: left;">
	<h2>Encrypt Data</h2>
	<form  action="" method="POST">
		<table>
			<tr>
				<td>
					Data Decrypt
				</td>
				<td>
					:
				</td>
				<td>
					<input type="text" name="en" id="en">
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<input style="float: right;" type="submit" name="submit" value="kirim">
				</td>
			</tr>
			<tr></tr>
			<?php 
			$en = $_POST['en'];
			$hasilen = base64_encode($en);
			?>
			<tr>
				<td>Hasil Encript</td>
				<td>:</td>
				<td><?php echo $hasilen; ?></td>
			</tr>
		</table>
	</form>
</div>

<div style="width: 50; float: left; padding: 0% 10%;">
	<h2>Decrypt Data</h2>
	<form  action="" method="POST">
		<table>
			<tr>
				<td>
					Data Encrypt
				</td>
				<td>
					:
				</td>
				<td>
					<input type="text" name="dec" id="dec">
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td>
					<input style="float: right;" type="submit" name="submit1" value="kirim">
				</td>
			</tr>
			<tr></tr>
			<?php 
			$dec = $_POST['dec'];
			$hasildec = base64_decode($dec);
			?>
			<tr>
				<td>Hasil Decrypt</td>
				<td>:</td>
				<td><?php echo $hasildec; ?></td>
			</tr>
		</table>
	</form>
</div>
</div>
<?php
    include "_partial/footer.php";
  ?>
</body>
</html>