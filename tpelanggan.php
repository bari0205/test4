 <?php
    include "koneksi.php";
  ?>
<!DOCTYPE>
<html>
<head>
	<title>Soal No. 4</title>

	<?php
    include "_partial/head.php";
  ?>
        
</head>
<body>
	<div style="text-align: center;">
		<h3>Tambah Pelanggan</h3>
	</div>
	<form class="form-horizontal" id="form" action="req_tpelanggan.php" method="POST">
		  <fieldset>
		    <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Kode Pelanggan</label>
		      <div class="col-lg-10">
		        <input type="text" class="form-control" id="kd_pel" name="kd_pel" value="KD"  >
		      </div>
		    </div>

		     <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Nama Pelanggan</label>
		      <div class="col-lg-10">
		        <input type="text" class="form-control" placeholder="Masukan Nama" id="nama_pel" name="nama_pel">
		      </div>
		    </div>

		     <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Alamat</label>
		      <div class="col-lg-10">
		      	<textarea class="form-control" name="alamat"></textarea>
		      </div>
		    </div>

		     <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">No. Hp</label>
		      <div class="col-lg-10">
		        <input type="number" class="form-control" placeholder="masukan No. Hp" id="no_hp" name="no_hp">
		      </div>
		    </div>
		    
		     
		    <div class="form-group">
		      <div class="col-lg-10 col-lg-offset-2">
		        <input type="submit" name="submit" class="btn btn-primary" value="Tambah">
		      </div>
		    </div>
		  </fieldset>
		</form> 
	<?php
    include "_partial/footer.php";
  ?>
</body>
	
</html>