 <?php
    include "koneksi.php";
  ?>
<!DOCTYPE>
<html>
<head>
	<title>Soal No. 4</title>

	<?php
    include "_partial/head.php";
  ?>
        
</head>
<body>
	<div style="text-align: center;">
		<h3>Tambah Produk</h3>
	</div>
	<form class="form-horizontal" id="form" action="req_tproduk.php" method="POST" enctype="multipart/form-data">
		  <fieldset>
		    <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Kode Produk</label>
		      <div class="col-lg-10">
		        <input type="text" class="form-control" id="kd_pro" name="kd_pro" value="PD"  >
		      </div>
		    </div>

		     <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Nama Produk</label>
		      <div class="col-lg-10">
		        <input type="text" class="form-control" placeholder="Masukan Nama" id="nama_pro" name="nama_pro">
		      </div>
		    </div>

		    
			<div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Brand</label>
		      <div class="col-lg-10">
			<select class="form-control" name="brand">
				<?php $query1=mysqli_query($konek,"SELECT *FROM brand ") or die (mysqli_error($konek));
		while($data1=mysqli_fetch_assoc($query1)){ ?>
				<option value="<?php echo $data1['code_brand'] ?>"><?php echo $data1['name'] ?></option>
			<?php } ?>
			</select>

			</div>
		    </div>

		    <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Gambar Produk</label>
		      <div class="col-lg-10">
		      	<input type="file" class="form-control" name="gambar" id="gambar">
		      </div>
		    </div>

		     <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Warna Produk</label>
		      <div class="col-lg-10">
		        <input type="text" class="form-control" placeholder="Masukan Warna" id="warna" name="warna">
		      </div>
		    </div>

		     <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Spesifikasi</label>
		      <div class="col-lg-10">
		      	<textarea class="form-control" name="spek"></textarea>
		      </div>
		    </div>

		    <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Harga</label>
		      <div class="col-lg-10">
		        <input type="number" class="form-control" placeholder="masukan Harga" id="harga" name="harga">
		      </div>
		    </div>

		     <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Qty</label>
		      <div class="col-lg-10">
		        <input type="number" class="form-control" placeholder="masukan qty" id="qty" name="qty">
		      </div>
		    </div>
		    
		     
		    <div class="form-group">
		      <div class="col-lg-10 col-lg-offset-2">
		        <input type="submit" name="submit" class="btn btn-primary" value="Tambah">
		      </div>
		    </div>
		  </fieldset>
		</form> 
	<?php
    include "_partial/footer.php";
  ?>
</body>
	
</html>