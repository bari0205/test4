 <?php
    include "koneksi.php";
  ?>
<!DOCTYPE>
<html>
<head>
	<title>Soal No. 4</title>

	<?php
    include "_partial/head.php";
  ?>
        
</head>
<body>
	<?php 
	$kode = $_GET['id'];
	?>
	<div style="text-align: center;">
		<h3>Tambah Stok</h3>
	</div>
	<form class="form-horizontal" id="form" action="req_tambah.php" method="POST" enctype="multipart/form-data">
		  <fieldset>
		    <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Kode Produk</label>
		      <div class="col-lg-10">
		        <input type="text" class="form-control" id="kd_pro" name="kd_pro" value="<?php echo $kode ?>" readonly="" >
		      </div>
		    </div>

		     <div class="form-group">
		      <label for="inputEmail" class="col-lg-2 control-label">Jumlah Produk</label>
		      <div class="col-lg-10">
		        <input type="text" class="form-control" placeholder="Masukan Nama" id="qty" name="qty">
		      </div>
		    </div>

		    <input type="text" name="tanggal" value="<?php echo date('Y-m-d H:i:s')?>" hidden>
		     
		    <div class="form-group">
		      <div class="col-lg-10 col-lg-offset-2">
		        <input type="submit" name="submit" class="btn btn-primary" value="Tambah">
		      </div>
		    </div>
		  </fieldset>
		</form> 
	<?php
    include "_partial/footer.php";
  ?>
</body>
	
</html>